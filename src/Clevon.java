// Importation des extentions pour lire et écrire des fichiers .csv et .txt
import extensions.CSVFile;
import extensions.File;


class Clevon extends Program { 

    final CSVFile QUESTIONS = loadCSV("../ressources/questions.csv");
    final CSVFile PARTIES = loadCSV("../ressources/parties.csv");
    final String SCENES_DE = "../ressources/txt/de.txt";
    final String SCENES = "../ressources/txt/scenes.txt";
    final String REGLES = "../ressources/txt/regles.txt";
    final int FACES = 6;
    final int COUPS = 5;
    final int NB_QUESTIONS_GAGNER = 10;
    final int NB_QUESTIONS_REPONDUES = 13;

    // // // // // // // // // // // // // //
    // == Fonctions relatives au joueur == //

    // Créer le profil du joueur en utilisant la classe Joueur
    Joueur creerProfilJoueur(CSVFile fichier, String prenom) {
        Joueur res = new Joueur();
        res.pseudo = prenom;
        res.idJoueur = rowCount(fichier);
        res.score = 0;
        res.questionsRepondues = new int[NB_QUESTIONS_REPONDUES];
        return res;
    }


    // // // // // // // //

    // Vérifie si la sauvegarde existe
    boolean existeSauvegarde(CSVFile fichier, int numeroPartie) {
        boolean res = false;
        // Si le numéro de sauvegarde est un numéro de ligne valide du fichier (sauf 0)
        if (numeroPartie > 0 && numeroPartie < rowCount(fichier)) {
            res = true;
        }
        return res;
    }


    // // // // // // // //

    // Récupérer les données du fichier des parties pour sauvegarder
    String[][] recuperationDonnees(CSVFile fichier) {
        // Créer un tableau avec le nombre de lignes du fichier .csv + 1 ligne pour la sauvegarde
        String[][] res = new String[rowCount(fichier)+1][columnCount(fichier)];
        // Remplir le tableau avec les données du fichier
        for (int li=0; li<length(res,1)-1; li++) {
            for (int col=0; col<length(res,2); col++) {
                res[li][col] = getCell(fichier, li, col);
            }
        }
        return res;
    }

    // Sauvegarder les informations du joueur
    void sauvegarde(CSVFile fichier, Joueur j) {
        // Récupérer les sauvegardes précédentes
        String[][] s = recuperationDonnees(fichier);
        // Ajouter les informations sur la ligne finale du tableau
        s[length(s,1)-1][0] = "" + rowCount(fichier);
        s[length(s,1)-1][1] = j.pseudo;
        s[length(s,1)-1][2] = "" + j.score;
        s[length(s,1)-1][3] = "";
        int idx = 0;
        // Concaténer les questions déjà visitées (id-id-id-)
        while (idx < length(j.questionsRepondues)) {
            s[rowCount(fichier)][3] = s[rowCount(fichier)][3] + j.questionsRepondues[idx] + "-";
            idx = idx + 1;
        }
        saveCSV(s, "../ressources/parties.csv");
    }

    void afficherParties(CSVFile fichier) {
        String id, pseudo, score;
        if (rowCount(fichier) == 1) {
            println("Aucune sauvegarde disponible. Merci de relancer le jeu ([Ctrl] + [C] pour stopper le programme).");
        }
        else {
            for (int li=1; li<rowCount(fichier); li++) {
                id = getCell(fichier, li, 0);
                pseudo = getCell(fichier, li, 1);
                score = getCell(fichier, li, 2);
                println("  . Emplacement n°" + id + ": " + pseudo + " avec un score de " + score);
            }
            print("Tu m'avais déjà aidé la dernière fois ? Quel est ton numéro de partie ? ");
        }
    }

    void sauvegardeAuto(Joueur j) {
        if (j.score == 5) {
            sauvegarde(PARTIES, j);
            nettoyageEcran();
            println("  . Sauvegarde effectuée au numéro " + rowCount(PARTIES));
            delay(3000);
        }
    }
    // // // // // // // //

    // Charger les données du joueur
    Joueur chargerProfilJoueur(CSVFile fichier, int numeroPartie) {
        Joueur res = new Joueur();
        res.pseudo = getCell(fichier, numeroPartie, 1);
        res.idJoueur = numeroPartie;
        res.score = stringToInt(getCell(fichier, numeroPartie, 2));
        res.questionsRepondues = new int[NB_QUESTIONS_REPONDUES];
        String questionsRepondues = getCell(fichier, numeroPartie, 3);
        int idxTab = 0;
        String strIdQuestion = "";
        for (int i=0; i<length(questionsRepondues); i++) {
            if (charAt(questionsRepondues, i) != '-') {
                strIdQuestion = strIdQuestion + charAt(questionsRepondues, i);
            }
            else {
                res.questionsRepondues[idxTab] = stringToInt(strIdQuestion);
                strIdQuestion = "";
                idxTab = idxTab + 1;
            }
        }
        return res;
    }


    // Vérifie que la réponse donnée est un nombre
    boolean estSauvegardeValide(String reponse, CSVFile fichier) {
        boolean res = true;
        int idx = 0;
        if (length(reponse) > 0) {
            while (res && idx < length(reponse)) {
                if (charAt(reponse, idx) < '0' || charAt(reponse, idx) > '9') {
                    res = false;
                }
                idx = idx + 1;
            }
            if (res) {
                if (stringToInt(reponse) <= 0 || stringToInt(reponse) >= rowCount(fichier)) {
                    res = false;
                } 
            }
        }
        else {
            res = false;
        }
        return res;
    }
    
    // // // // // // // //

    // Compte le nombre de questions répondues pour un joueur sauvegardé
    int nombreQuestionsReponduesSauvegarde(String questions) {
        int res = 0;
        for (int i=0; i<length(questions); i++) {
            if (charAt(questions,i) == '-') {
                res = res+1;
            }
        }
        return res;
    }


    // // // // // // // //

    // Vérifie si le joueur peut encore répondre à des questions (j.questionsRepondues non plein)
    boolean peutEncoreJouer(Joueur j) {
        boolean res = false;
        int idx = 0;
        while (!res && idx < length(j.questionsRepondues)) {
            if (j.questionsRepondues[idx] == 0) {
                res = true;
            }
            idx = idx + 1;
        }
        return res;
    }


    // // // // // // // // // // // // // // //
    // == Fonctions relatives aux questions == //

    // Ajoute la question au tableau des questions déjà répondues du joueur
    void ajoutQuestionRepondue(Joueur j, Question q) {
        if (peutEncoreJouer(j)) {
            boolean ajout = false;
            int idx = 0;
            while (!ajout && idx < length(j.questionsRepondues)) {
                if (j.questionsRepondues[idx] == 0) {
                    ajout = true;
                    j.questionsRepondues[idx] = q.idQuestion;
                }
                idx = idx + 1;
            }
        }
    }


    // // // // // // // //

    // Vérifie si la question choisie n'a pas déjà été répondue par le joueur
    boolean questionDejaRepondue(int idQuestion, Joueur j) {
        boolean res = false;
        int idx = 0;
        while (idx < length(j.questionsRepondues) && !res) {
            if (j.questionsRepondues[idx] == idQuestion) {
                res = true;
            }
            idx = idx + 1;
        }
        return res;
    }


    // // // // // // // //

    // Renvoie l'ID d'une question non répondue par le joueur prise aléatoirement dans le fichier .csv
    int choisirQuestion(Joueur j, CSVFile fichier) {
        int res;
        int nbLignes = rowCount(fichier);
        do {
            res = (int) (random() * (nbLignes-1) + 1);
        } while (questionDejaRepondue(res, j));
        return res;
    }


    // Compte le nombre de réponses possibles pour une question
    int reponsesPossibles(int idQuestion, CSVFile fichier) {
        int res = 0;
        for (int i=2; i<columnCount(fichier,idQuestion); i++) {
            if (!equals("",getCell(fichier,idQuestion,i))) {
                res = res + 1;
            }
        }
        return res;
    }


    // Crée la variable question à partir de l'ID choisi
    Question creerQuestion(int idQuestion, CSVFile fichier) {
        Question res = new Question();
        res.idQuestion = idQuestion;
        res.type = getCell(fichier, idQuestion, 0);
        res.enonce = getCell(fichier, idQuestion, 1);
        int nombreReponses = reponsesPossibles(idQuestion, fichier);
        res.reponses = new String[nombreReponses];
        for (int i=0; i<nombreReponses; i++) {
            res.reponses[i] = getCell(fichier, idQuestion, 2+i);
        }
        return res;
    }


    // // // // // // // //

    // Affiche la question et ses réponses si c'est un QCM
    void afficherQuestion(Question q, Joueur j) {
        String enoncee = q.enonce;
        println(enoncee);
        if (equals("QCM", q.type)) {
            int idx = 0;
            while (idx < length(q.reponses)) {
                println(((char) ('A' + idx)) + " : " + q.reponses[idx]);
                idx = idx + 1;   
            }
        }
        println("Clevon : Quelle est ta réponse, " + j.pseudo + " ?");
        print(j.pseudo + " : ");
    }


    // // // // // // // //

    // Mélanger les réponses dans la question
    void melanger(Question q) {
        int num = 1;
        String temp;
        if(equals("QCM",q.type)){
            num = (int) (random()*(length(q.reponses)-1));
            temp = q.reponses[num];
            q.reponses[num] = q.reponses[0];
            q.reponses[0] = temp;
        }
    }


    // // // // // // // // //

    // // // // // // // // // // // // // // // // //
    // == Fonctions relatives à la réponse donnée == //

    // Vérifie le bon format de la réponse
    
    boolean estAuBonFormat(Question q, String reponse) {
        boolean res = false;
        reponse = toUpperCase(reponse);
        // Si la question est un QCM
        if(equals(q.type,"QCM")){
            // Vérifier que la réponse ne soit pas vide
            if(length(reponse)==1){
                if(charAt(reponse,0) >= 'A' && charAt(reponse,0) < (char)(length(q.reponses)+'A')){
                    res = true;
                }
            }
        }
        // Si la question est libre
        else {
            if(!equals(reponse,"")){
                res = true;
            }
        }
        return res;
    }


    // // // // // // // // //

    // Redemande une réponse valide
    String redemander(String reponse,Question q, Joueur j){
        String res=reponse;
        while(!estAuBonFormat(q,res)){
            println("Clevon : Je te rappelle qu'il faut saisir une lettre entre celles de toutes les réponses proposées ! Réessaye, s'il-te-plaît.");
            print(j.pseudo + " : ");
            res = readString();
        }
        return res;
    }


    // // // // // // // // //

    // Vérifie si la réponse donnée est correcte
    boolean estBonneReponse(Question q, String reponse, CSVFile fichier){
        boolean res = false;
        reponse = toUpperCase(reponse);
        if(equals(q.type,"LIBRE")) {
            if(equals(toUpperCase(reponse), toUpperCase(q.reponses[0]))) {
                res = true;
            }
        }
        else {
            int indiceRep = (int)(charAt(reponse,0) -'A');
            if(equals(q.reponses[indiceRep],getCell(fichier,q.idQuestion,2))){
                res = true;
            }
        }
        return res;
    }


    // // // // // // // // // // // // // // 
    //== Fonctions relatives au mini-jeu == //

    // Initialise le jeu du dé
    De initialiserPartie() {
        De res = new De();
        res.nombreFaces = FACES;
        res.coups = COUPS;
        int max = (int) (random() * (FACES*COUPS));
        res.bornes = new int[]{max-FACES, max};
        return res;
    }


    // // // // // // // // //

    // Vérifie l'opérateur
    String verifierSaisie(String s) {
        while (!equals(s,"+") && !equals(s,"-")) {
            print(" > Erreur de saisie. Tu dois saisir '+' ou '-' : ");
            s = readString();
        } 
        return s;
    }


    // // // // // // // // //

    // Vérifie la prédiction
    String verifierPrediction(String s) {
        while (!equals(s,"O") && !equals(s,"N")) {
            print(" > Erreur de saisie. Tu dois saisir 'O' ou 'N' : ");
            s = readString();
        }
        return s;
    }


    // // // // // // // // //

    // Retourne une valeur du dé
    int lancerDe(De d) {
        return (int) (random() * (FACES-1)+1);
    } 


    // // // // // // // // //

    // Actualise le score
    int actualiserScore(int score, String operateur, int valeur) {
        if (equals(operateur, "+")) {
            score = score + valeur;
        }
        else {
            score = score - valeur;
        }
        return score;
    }


    // // // // // // // // //

    // Retourne le résultat d'une partie de jeu de dé
    boolean jeuDuDe() {
        boolean resultat = false;
        De d = initialiserPartie();
        int score = 0;
        String s;
        int valeur;
        nettoyageEcran();
        afficherScene(0, SCENES_DE);
        print("Penses-tu faire un score entre " + d.bornes[0] + " et " + d.bornes[1] + " en " + COUPS + " coups ? [O/N]\n > ");
        String prediction = verifierPrediction(readString());
        while (d.coups > 0) {
            d.coups = d.coups - 1;
            println("Objectif : somme comprise entre " + d.bornes[0] + " et " + d.bornes[1]);
            print(" > Saisie l'opération [+/-]: ");
            s = verifierSaisie(readString());
            valeur = lancerDe(d);
            nettoyageEcran();
            afficherScene(valeur, SCENES_DE);
            if (equals(s,"+")) {
                println("Tu additionnes " + valeur + " avec " + score + ".");
            }
            else {
                println("Tu soustrais " + valeur + " avec " + score + ".");
            }
            score = actualiserScore(score, s, valeur);
            println("Tu obtiens un score de " + score + ".");
            println("Il te reste " + d.coups + " coups.");
        }
        nettoyageEcran();
        if (score <= d.bornes[1] && score >= d.bornes[0] && equals(prediction, "O")) {
            afficherScene(8, SCENES_DE);
            println("Bravo tu as réussi !");
            resultat = true;
            compteARebours("Retour au jeu dans ", 5);
        }
        else if ((score >= d.bornes[1] || score <= d.bornes[0]) && equals(prediction, "N")) {
            afficherScene(8, SCENES_DE);
            println("Bravo, tu as fais le bon choix car tu es hors limite.");
            resultat = true;
            compteARebours("Retour au jeu dans ", 5);
        }
        else {
            afficherScene(7, SCENES_DE);
            println("Dommage... Tu as trop joué avec le feu...");
        }
        return resultat;
    }

    // // // // // // // // // // // // // // //
    //== Fonctions relatives à l'affichage == //
    
    // Affiche la scène spécifiée à l'écran
    void afficherScene(int scene, String chemin) {
        File fichier = newFile(chemin);
        int compteurScene = -1;
        boolean fin = false;
        // Parcours du fichier
        while(ready(fichier) && !fin) { 
            String ligne = readLine(fichier);
            // Si la ligne vaut DEBUT alors c'est une nouvelle scène
            if (equals(ligne, "Debut")){
                compteurScene = compteurScene + 1;
            }
            // Si c'est la fin de la scène précédente à celle voulue alors on se prépare à afficher
            else if (equals(ligne, "Fin") && compteurScene == scene) {
                fin = true;
            }
            // On affiche la scène
            else if (compteurScene == scene) {
                println(ligne);
            }
        }
    }


    // // // // // // // // //

    // Renvoie le choix de navigation après avoir affiché le menu
    int menuNavigation() {
        println("=== Menu principal ===");
        String[] menu = new String[]{"Jouer","Les règles et l'histoire","Charger une partie","Quitter"};
        // Affichage du menu
        for (int i=0; i<length(menu); i++) {
            println((i+1) + ") " + menu[i]);
        }
        print("Que veux-tu faire ? ");
        String saisie = readString();
        boolean estValide = false;
        // Vérification de la validité de la saisie
        while (!estValide) {
            if (length(saisie)== 1 && '1' <= charAt(saisie,0) && charAt(saisie,0) <= (char) (length(menu)+'0')) {
                estValide = true;
            } 
            else {
                print("Attention aventurier, tu dois saisir un chiffre compris entre 1 et " + length(menu) + " : ");
                saisie = readString();
            }
        }
        return stringToInt(saisie);
    }


    // // // // // // // // //

    // Effectue le compte à rebours en affichant une phrase
    void compteARebours(String phrase, int temps) {
        print(phrase);
        for (int i=0; i<temps; i++) {
            print(" " + (temps-i));
            delay(1000);
        }
        println();
    }
    

    // // // // // // // // //

    // Efface le contenu de l'écran et replace le curseur en position initiale
    void nettoyageEcran() {
        clearScreen();
        cursor(0,0);
    }

    // // // // // // // // // // // // //
    // == Fonctions relatives au jeu == //

    // Vérifie si le fichier ne comporte pas de colonne manquante
    boolean estFichierProfilCorrect(){
        boolean res = true;
        int idxL = 0;
        while (res && idxL < rowCount(PARTIES)) {
            if (columnCount(PARTIES,idxL) != 4) {
                res = false;
                println("Une erreur a été détectée ligne " + idxL + " (valeur.s manquante.s)");
            }
            idxL = idxL + 1;
        }
        return res;
    }

    boolean estFichierQuestionsCorrect() {
        int nbLignes = rowCount(QUESTIONS);
        if (nbLignes >= NB_QUESTIONS_REPONDUES+1) {
            return true;
        }
        else {
            println("Erreur dans le fichier des questions : il en manque " + (NB_QUESTIONS_REPONDUES-nbLignes));
            return false;
        }
    }

    // Fait jouer le joueur en affichant les bonnes scènes
    void jeu(Joueur j, int compteurScene, boolean derniereChance) {
        Question q;
        String reponse;
        while (j.score < NB_QUESTIONS_GAGNER && peutEncoreJouer(j)) {
            sauvegardeAuto(j);
            nettoyageEcran();
            println("> Score : " + j.score + "/" + NB_QUESTIONS_GAGNER);
            afficherScene(compteurScene, SCENES);
            q = creerQuestion(choisirQuestion(j, QUESTIONS), QUESTIONS);
            melanger(q);
            ajoutQuestionRepondue(j, q);
            afficherQuestion(q,j);
            reponse = readString();
            while (!estAuBonFormat(q, reponse)) {
                reponse = redemander(reponse, q, j);
            }
            if (estBonneReponse(q, reponse, QUESTIONS)) {
                j.score = j.score + 1;
                compteurScene = compteurScene + 1;
                println("Clevon : Bonne réponse ! Nous pouvons avancer !");
            }
            else {
                println("Clevon : Dommage ! La bonne réponse était : " + getCell(QUESTIONS, q.idQuestion, 2));
            }
            if (j.score < NB_QUESTIONS_GAGNER) {
                compteARebours("Question suivante dans ", 5);
            }
        }
        nettoyageEcran();
        if (j.score == NB_QUESTIONS_GAGNER) {
            afficherScene(1, SCENES);
            println("Clevon : Félicitations ! Grâce à ton aide nous sommes arrivés à l'école ! Croire en soi est la clé pour atteindre ses objectifs, qu'importe comment tu es ou d'où tu viens :) <3 keur keur keur");
        }
        else {
            if (j.score == NB_QUESTIONS_GAGNER-1 && derniereChance) {
                // Jeu du dé
                if (jeuDuDe()) {
                    nettoyageEcran();
                    j.questionsRepondues[length(j.questionsRepondues)-1] = 0;
                    derniereChance = false;
                    jeu(j, compteurScene, derniereChance);
                }
                else {
                    println("Clevon : Dommage, nous n'avons pas été assez malins...Nous y arriverons une prochaine fois, peut-être !");
                }
            }
            else {
                println("Clevon : Dommage, nous n'avons pas été assez malins...Nous y arriverons une prochaine fois, peut-être !");
            }
        }
        sauvegarde(PARTIES, j);
    }


    // // // // // // // // // // //
    // == Algorithme principal == //

    void algorithm() {
        nettoyageEcran();
        boolean partiesCorrectes = estFichierProfilCorrect();
        boolean questionsCorrectes = estFichierQuestionsCorrect();
        if (partiesCorrectes && questionsCorrectes) {
            afficherScene(0, SCENES);

            Joueur j = creerProfilJoueur(PARTIES, "");

            int choixNavigation = menuNavigation();
            while (choixNavigation != 1 && choixNavigation != 4) {
                if (choixNavigation == 2) {
                    nettoyageEcran();
                    afficherScene(0, REGLES);
                    print("Appuie sur [Enter] pour continuer. ");
                    String sortie = readString();
                    nettoyageEcran();
                    afficherScene(0, SCENES);
                    choixNavigation = menuNavigation();
                }
                else if (choixNavigation == 3) {
                    afficherParties(PARTIES);
                    String sauvegarde = readString();
                    boolean sauvegardeValide;
                    int idJoueur = 0;
                    do {
                        if (!estSauvegardeValide(sauvegarde, PARTIES)) {
                            sauvegardeValide = false;
                            print("Sauvegarde invalide, réessaie : ");
                            sauvegarde = readString();
                        }
                        else {
                            sauvegardeValide = true;
                            idJoueur = stringToInt(sauvegarde);
                        }
                    } while (!sauvegardeValide);
                    j = chargerProfilJoueur(PARTIES, idJoueur);
                    choixNavigation = 1;
                    println("Ravi de te revoir, " + j.pseudo + "! Reprenons...");
                    compteARebours("Reprise du jeu dans ", 5);
                }
                
            }
            if (choixNavigation == 1) {
                Question q;
                String reponse = "";
                int compteurScene = 1;
                if (j.pseudo == "") {
                    println("Bonjour toi ! Moi c'est Clevon, j'ai de toutes petites ailes et je dois finir cette course. J'ai besoin de ton aide...");
                    print("Mais d'abord, quel est ton nom ? ");
                    String prenom = readString();
                    while (length(prenom) == 0) {
                        print("Le silence n'est pas un prénom... Quel est ton prénom ? ");
                        prenom = readString();
                    }
                    println("C'est joli ça, " + prenom + ".");
                    compteARebours("Commençons vite...", 3);
                    j = creerProfilJoueur(PARTIES, prenom);
                }
                else {
                    compteurScene = j.score;
                }
                jeu(j, compteurScene+1, true);
            }
            else if (choixNavigation == 4) {
                println("Tu ne m'as été d'aucune aide, aventurier, à bientôt !");
            }
        }
    }

    // // // // // // // // // //
    // == Fonctions de test == //

    // Clevon

    void testCreerProfilJoueur() {
        Joueur test = creerProfilJoueur(PARTIES, "Michel");
        println("\n" + test.pseudo + "#" + test.idJoueur + " a bien été créé !");
        println("Score initial = " + test.score);
        print("Questions déjà répondues : ");
        for (int i=0; i<length(test.questionsRepondues); i++) {
            print(test.questionsRepondues[i]);
        }
        println();
    }
    void testEstBonneReponse() {
        Question q = creerQuestion(1, QUESTIONS);
        assertTrue(estBonneReponse(q, "A", QUESTIONS));
        assertFalse(estBonneReponse(q, "B", QUESTIONS));
        assertFalse(estBonneReponse(q, "D", QUESTIONS));
        Question q2 = creerQuestion(10, QUESTIONS);
        assertTrue(estBonneReponse(q2,"Louis XIV", QUESTIONS));
        assertFalse(estBonneReponse(q2,"Louis X", QUESTIONS));
    }
    void testExisteSauvegarde() {
        assertTrue(existeSauvegarde(PARTIES, 1));
        assertFalse(existeSauvegarde(PARTIES, 0));
        assertFalse(existeSauvegarde(PARTIES, -450));
        assertFalse(existeSauvegarde(PARTIES, 450));
    }
    void testSauvegarde() {
        Joueur j = chargerProfilJoueur(PARTIES, 1);
        // sauvegarde(PARTIES, j);
    }
    void testChargerProfilJoueur() {
        Joueur j = chargerProfilJoueur(PARTIES, 1);
        assertEquals("John", j.pseudo);
        int[] tab = new int[]{1,4,0,0,0,0,0,0};
        assertArrayEquals(j.questionsRepondues, tab);
    }
    void testEstSauvegardeValide() {
        assertTrue(estSauvegardeValide("1", PARTIES));
        assertFalse(estSauvegardeValide("400", PARTIES));
        assertFalse(estSauvegardeValide("0", PARTIES));
        assertFalse(estSauvegardeValide("", PARTIES));
        assertFalse(estSauvegardeValide("1a", PARTIES));
    }
    void testNombreQuestionsReponduesSauvegarde() {
        assertEquals(3, nombreQuestionsReponduesSauvegarde("1-12-3-"));
        assertEquals(0, nombreQuestionsReponduesSauvegarde(""));
        assertEquals(1, nombreQuestionsReponduesSauvegarde("14-"));
    }
    void testPeutEncoreJouer() {
        Joueur j = creerProfilJoueur(PARTIES, "Michel");
        assertTrue(peutEncoreJouer(j));
        for (int i=0; i<length(j.questionsRepondues); i++) {
            j.questionsRepondues[i] = choisirQuestion(j, QUESTIONS);
        }
        assertFalse(peutEncoreJouer(j));
    }
    void testAjoutQuestionRepondue() {
        Joueur j = creerProfilJoueur(PARTIES, "Michel");
        Question q = creerQuestion(choisirQuestion(j, QUESTIONS), QUESTIONS);
        int[] tabTest = new int[]{q.idQuestion,0,0,0,0,0,0,0};
        ajoutQuestionRepondue(j, q);
        assertArrayEquals(j.questionsRepondues,tabTest);
    }
    void testQuestionDejaRepondue() {
        Joueur j = creerProfilJoueur(PARTIES, "Michel");
        j.questionsRepondues[0] = 5;
        assertTrue(questionDejaRepondue(5, j));
        assertFalse(questionDejaRepondue(1, j));
    }
    void testChoisirQuestion() {
        Joueur j = creerProfilJoueur(PARTIES, "Michel");
        j.questionsRepondues[0] = 5;
        for (int i=0; i<10; i++) {
            assertNotEquals(5, choisirQuestion(j, QUESTIONS));
        }
    }
    void testCreerQuestion() {
        Joueur j = creerProfilJoueur(PARTIES,"Michel");
        Question q = creerQuestion(7,QUESTIONS);
        String[] tabVerif = new String[]{"VRAI","FAUX"};
        assertArrayEquals(tabVerif, q.reponses);
    }
    void testReponsesPossibles() {
        assertEquals(4, reponsesPossibles(1, QUESTIONS));
        assertEquals(2, reponsesPossibles(7, QUESTIONS));
        assertEquals(1, reponsesPossibles(10, QUESTIONS));
    }
    void testAfficherQuestion() {
        Question q = creerQuestion(1, QUESTIONS);
        Joueur j = creerProfilJoueur(PARTIES, "Michel");
        println();
        afficherQuestion(q,j);
    }
    void testMelanger() {
        println("=== QUESTION D'ORIGINE ===");
        Question test = creerQuestion(1, QUESTIONS);
        println("Question n°" + test.idQuestion + " : " + test.enonce);
        if (equals(test.type, "QCM")) {
            println("Réponses possibles : ");
            for (int i=0; i<length(test.reponses); i++) {
                println("   -" + (char) (i + 'A') + " " + test.reponses[i]);
            }
        }
        println("======================\n=== QUESTION MELANGEE ===");
        println("Question n°" + test.idQuestion + " : " + test.enonce);
        melanger(test);
        if (equals(test.type, "QCM")) {
            println("Réponses possibles : ");
            for (int i=0; i<length(test.reponses); i++) {
                println("   -" + (char) (i + 'A') + " " + test.reponses[i]);
            }
        }
    }
    void testEstAuBonFormat() {
        Question q = creerQuestion(1, QUESTIONS);
        assertTrue(estAuBonFormat(q,"A"));
        assertFalse(estAuBonFormat(q,"E"));
        Question q1 = creerQuestion(10, QUESTIONS);
        assertTrue(estAuBonFormat(q1,"oui"));
        assertFalse(estAuBonFormat(q1,""));
    }

    // Jeu du dé

    void testVerifierSaisie() {
        String s = "a";
        s = verifierSaisie(s);
    }
    void testVerifierPrediction() {
        String s = "a";
        s = verifierPrediction(s);
    }
    void testLancerDe() {
        De d = initialiserPartie();
        assertGreaterThanOrEqual(1, lancerDe(d));
        assertLessThanOrEqual(6, lancerDe(d));
    }
    void testActualiserScore() {
        assertEquals(8, actualiserScore(6, "+", 2));
        assertEquals(5, actualiserScore(8, "-", 3));
        assertEquals(1, actualiserScore(-4, "+", 5));
    }
}