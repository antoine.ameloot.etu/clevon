﻿ # CLEVON
## Presentation du jeu
Clevon est un logiciel ludo-pédagogique conçu pour les enfants d'école primaire.Le but est d'incarner Clevon, un dragon ayant la particularité de posséder de petites ailes contrairement à ses congénères : de ce fait, il devra à l'aide du joueur répondre à des questions afin de franchir les divers obstacles sur le parcours.

## Lancement du jeu

Afin d'accéder à Clevon, il sera nécessaire d'utiliser les deux commandes suivantes sur un terminal :

>./compile.sh (compiler les fichiers présents dans 'src')

> ./run.sh Clevon (executer le jeu)

## Ajout de questions supplémentaires

Si vous souhaitez personnaliser le jeu, il vous est possible de le compléter en ajoutant les questions de votre choix : dans le dossier ressources, ouvrez le fichier ''questions.csv'' et remplissez sur une nouvelle ligne, **de gauche à droite** :

→   Type de question (QCM ou LIBRE **en majuscules**)

→  Question souhaitée

→  Au moins une bonne réponse et une mauvaise réponse (cas d'un QCM)

→  Une bonne réponse (cas d'une question LIBRE)

### Attention ! /!\
- Chaque élément doit être séparé par une **virgule**.

- Dans le cas d'un QCM, si toutes les colonnes ne sont pas remplies, il est nécessaire d'ajouter un **total de 5 virgules**

>Ex : Une question libre avec un bonne réponse, on comble les espaces vides avec des virgules

>LIBRE, Saisir rouge,rouge,,,

